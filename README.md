# Put It In Park

Provides information about national parks in the United States.

### Website
The project website is putitinpark.xyz (or putitinpark.me)

## Group Members
| Name  | EID | GitLab Username |
| ------------- | ------------- | ------------- |
| Miles Chandler | mac9325 | mchandle |
| Joseph Engelhart | jae3362 | jengelhart |
| Ella Robertson | ear3294 | ella.robertson |
| Billy Vo | bv5433 | billyvo |
| Levi Villarreal | lrv432  | leviv1 |

## Git SHA
| Phase | Git SHA |
| ------------- | ------------- |
| Phase I | 78d4ab8333b4fde557779021f0f55ce1bfc782b1 |
| Phase II | 0b18ebfb3349a89296881762f59674e02156f243 |
| Phase III | 8bbaebb7f94b7a9b803ab4284765129e7fac82de |
| Phase IV | f6b4b2a21cfd68d9c5415c1dbb4e9721b1c69a3d |

## GitLab Pipelines

The GitLab pipelies of the project can be found [here](https://gitlab.com/leviv1/putitinpark/pipelines).

## Completion Times (per member)
| Phase | Estimated Completion Time | Actual Completion Time |
| ------------- | ------------- | ------------- |
| I | 15 hours | 18 hours |
| II | 22 hours | 52 hours |
| III | 40 hours | 42 hours |
| IV | 30 hours | 30 hours |

## Notes
